//
//  NetworkDataProvider.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/13/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import Moya

class NetworkDataProvider: NetworkDataProviderProtocol {
    private let provider = MoyaProvider<CryptoCompare>()
    private let appCurrency = "USD"

    func provideFullDataOf(currencies: [String], completion: ((Error?, FullDataResponse?) -> Void)?) {
        let currenciesJoined = currencies.joined(separator: ",")
        let fullDataWithParameters = CryptoCompare.fullData(fromSymbols: currenciesJoined,
                                                            toSymbols: appCurrency)
        provider.request(fullDataWithParameters) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    _ = try moyaResponse.filterSuccessfulStatusCodes()
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(FullDataResponse.self, from: moyaResponse.data)
                    completion?(nil, response)
                } catch {
                    completion?(DataParsingError.conversionError, nil)
                }
            case let .failure(error):
                completion?(error, nil)
            }
        }
    }

    func provideAllAvailableCurrencies(completion: ((Error?, AllCoinsResponse?) -> Void)?) {
        provider.request(.allCoins) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    _ = try moyaResponse.filterSuccessfulStatusCodes()
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(AllCoinsResponse.self, from: moyaResponse.data)
                    completion?(nil, response)
                } catch {
                    completion?(DataParsingError.conversionError, nil)
                }
            case let .failure(error):
                completion?(error, nil)
            }
        }
    }

    func provideMinuteHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?) {
        let fullDataWithParameters = CryptoCompare.historicalMinute(fromSymbol: currency,
                                                                    toSymbol: appCurrency,
                                                                    amount: amountOfTimestamps)
        historicalDataRequest(requestData: fullDataWithParameters, completion: completion)
    }

    func provideHourlyHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?) {
        let fullDataWithParameters = CryptoCompare.historicalHourly(fromSymbol: currency,
                                                                    toSymbol: appCurrency,
                                                                    amount: amountOfTimestamps)
        historicalDataRequest(requestData: fullDataWithParameters, completion: completion)
    }

    func provideDailyHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?) {
        let fullDataWithParameters = CryptoCompare.historicalDaily(fromSymbol: currency,
                                                                   toSymbol: appCurrency,
                                                                   amount: amountOfTimestamps)
        historicalDataRequest(requestData: fullDataWithParameters, completion: completion)
    }

    private func historicalDataRequest(requestData: CryptoCompare, completion: ((Error?, HistoricalDataResponse?) -> Void)?) {
        provider.request(requestData) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    _ = try moyaResponse.filterSuccessfulStatusCodes()
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .secondsSince1970
                    let response = try decoder.decode(HistoricalDataResponse.self, from: moyaResponse.data)
                    completion?(nil, response)
                } catch {
                    print(error)
                    completion?(DataParsingError.conversionError, nil)
                }
            case let .failure(error):
                completion?(error, nil)
            }
        }
    }
}
