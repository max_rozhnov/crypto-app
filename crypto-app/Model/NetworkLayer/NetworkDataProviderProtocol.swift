//
//  NetworkDataProviderProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/13/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol NetworkDataProviderProtocol {
    func provideFullDataOf(currencies: [String], completion: ((Error?, FullDataResponse?) -> Void)?)
    func provideAllAvailableCurrencies(completion: ((Error?, AllCoinsResponse?) -> Void)?)
    func provideMinuteHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?)
    func provideHourlyHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?)
    func provideDailyHistoricalDataOf(currency: String, amountOfTimestamps: Int, completion: ((Error?, HistoricalDataResponse?) -> Void)?)
}
