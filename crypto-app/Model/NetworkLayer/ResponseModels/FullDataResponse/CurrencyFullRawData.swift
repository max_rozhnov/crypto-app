//
//  CurrencyFullRawData.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/25/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct CurrencyFullRawData: Codable {
    let currencySymbol: String
    let price: Double
    let changeDay: Double
    let imageUrl: String

    private enum CodingKeys: String, CodingKey {
        case currencySymbol = "FROMSYMBOL"
        case price = "PRICE"
        case changeDay = "CHANGEDAY"
        case imageUrl = "IMAGEURL"
    }
}
