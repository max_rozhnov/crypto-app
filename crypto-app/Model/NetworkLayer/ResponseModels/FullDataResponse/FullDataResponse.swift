//
//  FullDataResponse.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/18/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct FullDataResponse: Codable {
    let display: [String: [String: CurrencyFullDisplayData]]
    let raw: [String: [String: CurrencyFullRawData]]

    private enum CodingKeys: String, CodingKey {
        case display = "DISPLAY"
        case raw = "RAW"
    }
}
