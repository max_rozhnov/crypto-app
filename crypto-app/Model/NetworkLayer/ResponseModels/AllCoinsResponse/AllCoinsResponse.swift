//
//  AllCoinsResponse.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/19/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct AllCoinsResponse: Codable {
    let baseImageUrl: String
    let allCoins: [String: OneCoinData]

    private enum CodingKeys: String, CodingKey {
        case baseImageUrl = "BaseImageUrl"
        case allCoins = "Data"
    }
}
