//
//  OneCoinData.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/25/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct OneCoinData: Codable {
    let imageUrl: String?
    let shortName: String
    let fullName: String
    let currencySymbol: String
    let isTrading: Bool

    private enum CodingKeys: String, CodingKey {
        case imageUrl = "ImageUrl"
        case shortName = "Name"
        case fullName = "CoinName"
        case currencySymbol = "Symbol"
        case isTrading = "IsTrading"
    }
}
