//
//  HistoryEntry.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/25/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct HistoryEntry: Codable {
    let time: Date
    let open: Double
    let high: Double
    let low: Double
    let close: Double
}
