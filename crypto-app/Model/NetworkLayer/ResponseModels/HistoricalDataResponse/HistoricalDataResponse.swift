//
//  HistoricalDataResponse.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/21/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct HistoricalDataResponse: Codable {
    let timeFrom: Date?
    let timeTo: Date?
    var historyEntries: [HistoryEntry]

    private enum CodingKeys: String, CodingKey {
        case timeFrom = "TimeFrom"
        case timeTo = "TimeTo"
        case historyEntries = "Data"
    }
}
