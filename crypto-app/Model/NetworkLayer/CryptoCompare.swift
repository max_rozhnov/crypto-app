//
//  CryptoCompare.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/6/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import Moya

enum CryptoCompare {
    static private let apiKey = "8d8beb440e69197dd8db1d6280e7234922dd3a044e99356bf1e5aa787bde7802"

    case fullData(fromSymbols: String, toSymbols: String)
    case historicalMinute(fromSymbol: String, toSymbol: String, amount: Int)
    case historicalHourly(fromSymbol: String, toSymbol: String, amount: Int)
    case historicalDaily(fromSymbol: String, toSymbol: String, amount: Int)
    case allCoins
}

extension CryptoCompare: TargetType {
    var baseURL: URL {
        if let url = URL(string: "https://min-api.cryptocompare.com/data") {
            return url
        } else {
            return URL(fileURLWithPath: "")
        }
    }
    var path: String {
        switch self {
        case .fullData:
            return "/pricemultifull"
        case .allCoins:
            return "/all/coinlist"
        case .historicalMinute:
            return "/histominute"
        case .historicalHourly:
            return "/histohour"
        case .historicalDaily:
            return "/histoday"
        }
    }
    var method: Moya.Method {
        switch self {
        case .fullData, .allCoins, .historicalMinute, .historicalHourly, .historicalDaily:
            return .get
        }
    }
    var task: Task {
        switch self {
        case let .fullData(fromSymbols, toSymbols):
            return .requestParameters(parameters: ["fsyms": fromSymbols, "tsyms": toSymbols], encoding: URLEncoding.queryString)
        case let .historicalMinute(fromSymbol, toSymbol, amount),
             let .historicalHourly(fromSymbol, toSymbol, amount),
             let .historicalDaily(fromSymbol, toSymbol, amount):
            return .requestParameters(parameters: ["fsym": fromSymbol, "tsym": toSymbol, "limit": amount], encoding: URLEncoding.queryString)
        case .allCoins:
            return .requestPlain
        }
    }
    var sampleData: Data {
        return Data()
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json",
                "Apikey": CryptoCompare.apiKey]
    }
}
