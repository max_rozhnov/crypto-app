//
//  CurrencyData.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/19/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct CurrencyData {
    var shortName: String
    var currentPrice: String
    var priceChangeDay: String
}
