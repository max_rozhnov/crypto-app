//
//  CurrencyModel.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/19/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol CurrencyModelProtocol {
    var appCurrency: String { get }

    func addTrackedCurrency(withShortName shortName: String)
    func getCurrenciesData(completion: ((Error?, FullDataResponse?) -> Void)?)
    func getAvailableCurrenciesData(completion: ((Error?, AllCoinsResponse?) -> Void)?)
    func getHistoricalData(of currency: String, for timePeriod: TimePeriod, completion: ((Error?, HistoricalDataResponse?) -> Void)?)
    func currenciesLocalData(completion: (([CurrencyData]?) -> Void)?)
    func historicalLocalData(of currency: String, for timePeriod: TimePeriod, completion: (([HistoryEntry]?) -> Void)?)
}
