//
//  CurrencyModelRemote.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/6/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import Moya

enum DataParsingError: Error {
    case conversionError
}

class CurrencyModelRemote: CurrencyModelProtocol {

    var appCurrency = "USD"

    private let provider = MoyaProvider<CryptoCompare>()
    private let networkDataProvider: NetworkDataProviderProtocol
    private let localStorageManager: LocalDataManagerProtocol
    private var trackedCurrenciesShortNames = ["LTC", "BCH", "ETH", "ETC"]

    init(networkDataProvider: NetworkDataProviderProtocol, localStorageManager: LocalDataManagerProtocol) {
        self.networkDataProvider = networkDataProvider
        self.localStorageManager = localStorageManager
    }

    func getCurrenciesData(completion: ((Error?, FullDataResponse?) -> Void)?) {
        networkDataProvider.provideFullDataOf(currencies: trackedCurrenciesShortNames) { error, retrievedData in
            if let data = retrievedData {
                self.cacheCurrencyData(data: data)
            }
            completion?(error, retrievedData)
        }
    }
    func getAvailableCurrenciesData(completion: ((Error?, AllCoinsResponse?) -> Void)?) {
        networkDataProvider.provideAllAvailableCurrencies { error, retrievedData in
            completion?(error, retrievedData)
        }
    }
    func getHistoricalData(of currency: String, for timePeriod: TimePeriod, completion: ((Error?, HistoricalDataResponse?) -> Void)?) {
        self.localStorageManager.amountOfTimestampsToLoadFor(currency: currency, for: timePeriod) { entriesToLoad in
            let historyRequestCallback: ((Error?, HistoricalDataResponse?) -> Void) = { [weak self] error, retrievedData in
                if let historicalData = retrievedData {
                    self?.localStorageManager.saveHistoricalDataOf(currency: currency,
                                                                   historicalData: historicalData,
                                                                   forTimePeriod: timePeriod) {
                        self?.localStorageManager.loadHistoricalDataOf(currency: currency,
                                                                       forTimePeriod: timePeriod) { cachedTimestamps in
                            let result = HistoricalDataResponse(timeFrom: Date(), timeTo: Date(), historyEntries: cachedTimestamps)
                            completion?(error, result)
                        }
                    }
                } else {
                    completion?(error, retrievedData)
                }
            }

            if entriesToLoad > 0 {
                switch timePeriod {
                case .minute:
                    self.networkDataProvider.provideMinuteHistoricalDataOf(currency: currency,
                                                                           amountOfTimestamps: entriesToLoad,
                                                                           completion: historyRequestCallback)
                case .hour:
                    self.networkDataProvider.provideHourlyHistoricalDataOf(currency: currency,
                                                                           amountOfTimestamps: entriesToLoad,
                                                                           completion: historyRequestCallback)
                case .day:
                    self.networkDataProvider.provideDailyHistoricalDataOf(currency: currency,
                                                                          amountOfTimestamps: entriesToLoad,
                                                                          completion: historyRequestCallback)
                }
            } else {
                self.localStorageManager.loadHistoricalDataOf(currency: currency, forTimePeriod: timePeriod) { cachedHistoryEntries in
                        let historicalData = HistoricalDataResponse(timeFrom: nil, timeTo: nil, historyEntries: cachedHistoryEntries)
                        completion?(nil, historicalData)
                }
            }
        }
    }
    func historicalLocalData(of currency: String, for timePeriod: TimePeriod, completion: (([HistoryEntry]?) -> Void)?) {
        localStorageManager.loadHistoricalDataOf(currency: currency, forTimePeriod: timePeriod) { cachedHistoryEntries in
            completion?(cachedHistoryEntries)
        }
    }
    func currenciesLocalData(completion: (([CurrencyData]?) -> Void)?) {
        localStorageManager.loadCurrencies(currencyNames: trackedCurrenciesShortNames) { localCurrencyData in
            completion?(localCurrencyData)
        }
    }
    func addTrackedCurrency(withShortName shortName: String) {

        if !trackedCurrenciesShortNames.contains(shortName) {
            trackedCurrenciesShortNames.append(shortName)
        }
    }

    private func cacheCurrencyData(data: FullDataResponse) {
        var currenciesToSave = [CurrencyData]()
        for (currencyName, currencyData) in data.display {
            if let currencySpecificData = currencyData[appCurrency] {
                let currencyPrice = currencySpecificData.price
                let currencyPriceChange = currencySpecificData.changeDay
                let currencyToAdd = CurrencyData(shortName: currencyName,
                                                 currentPrice: currencyPrice,
                                                 priceChangeDay: currencyPriceChange )
                currenciesToSave.append(currencyToAdd)
            }
        }
        self.localStorageManager.saveCurrenciesCollectionData(currencies: currenciesToSave)
    }
}
