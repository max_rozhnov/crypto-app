//
//  Timestamp.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/12/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit
import CoreData

class Timestamp: NSManagedObject {
    convenience init(fromHistoryEnrty historyEntry: HistoryEntry, context: NSManagedObjectContext) {
        self.init(context: context)
        self.high = historyEntry.high
        self.low = historyEntry.low
        self.open = historyEntry.open
        self.close = historyEntry.close
        self.time = historyEntry.time
    }

    func asHistoryEntry() -> HistoryEntry {
         return HistoryEntry(time: self.time ?? Date(),
                             open: self.open,
                             high: self.high,
                             low: self.low,
                             close: self.close)
    }
}
