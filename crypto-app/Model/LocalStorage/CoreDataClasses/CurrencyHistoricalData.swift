//
//  CurrencyHistoricalData.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/12/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit
import CoreData

class CurrencyHistoricalData: NSManagedObject {
    convenience init(withHistoricalData historicalData: HistoricalDataResponse,
                     context: NSManagedObjectContext,
                     currencyName: String,
                     timePeriod: String) {
        self.init(context: context)
        self.shortName = currencyName
        self.timePeriod = timePeriod
        for historyEntry in historicalData.historyEntries {
            self.addToTimestamps(Timestamp(fromHistoryEnrty: historyEntry, context: context))
        }
    }

    public func removeTimestampsBefore(date: Date) {
        if let currentTimestamps = self.timestamps {
            for timestamp in currentTimestamps {
                if let currentTimestamp = timestamp as? Timestamp,
                   let timestampDate = currentTimestamp.time,
                   timestampDate < date {
                    self.removeFromTimestamps(currentTimestamp)
                    let context = currentTimestamp.managedObjectContext
                    context?.delete(currentTimestamp)
                }
            }
        }
    }

    public func appendTimestamp(timestamp: Timestamp) {
        if let replacementIndex = self.indexOf(timestamp: timestamp) {
            if let timestampToReplace = self.timestamps?.object(at: replacementIndex) as? Timestamp {
                let context = timestampToReplace.managedObjectContext
                self.removeFromTimestamps(timestampToReplace)
                context?.delete(timestampToReplace)
            }
        }
        self.addToTimestamps(timestamp)
    }

    private func indexOf(timestamp: Timestamp) -> Int? {
        var index: Int?
        let containsTimestamp = self.timestamps?.contains { entry in
            if let unwrapped = entry as? Timestamp {
                index = timestamps?.index(of: entry)
                return unwrapped.time == timestamp.time
            } else {
                return false
            }
        } ?? false
        return containsTimestamp ? index : nil
    }
}
