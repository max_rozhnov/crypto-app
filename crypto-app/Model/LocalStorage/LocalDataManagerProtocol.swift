//
//  LocalDataProviderProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/9/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol LocalDataManagerProtocol {
    func saveCurrenciesCollectionData(currencies: [CurrencyData])
    func amountOfTimestampsToLoadFor(currency currencyName: String,
                                     for timePeriod: TimePeriod,
                                     completion: ((Int) -> Void)?)
    func loadCurrencies(currencyNames: [String],
                        completion: (([CurrencyData]) -> Void)?)
    func loadHistoricalDataOf(currency currencyName: String,
                              forTimePeriod timePeriod: TimePeriod,
                              completion: (([HistoryEntry]) -> Void)?)
    func saveHistoricalDataOf(currency currencyName: String,
                              historicalData: HistoricalDataResponse,
                              forTimePeriod timePeriod: TimePeriod,
                              completion: (() -> Void)?)
}
