//
//  LocalDataProvider.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/8/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class LocalDataManager: LocalDataManagerProtocol {
    let context: NSManagedObjectContext
    var amountOfTimestampsToKeep: Int = 60

    init(with context: NSManagedObjectContext) {
        self.context = context
    }

    func saveCurrenciesCollectionData(currencies: [CurrencyData]) {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        privateContext.perform {
            for currencyData in currencies {
                let predicate = NSPredicate(format: "shortName = %@", currencyData.shortName)
                let fetchRequest: NSFetchRequest<CurrencyInfo> = CurrencyInfo.fetchRequest()
                fetchRequest.predicate = predicate
                fetchRequest.returnsObjectsAsFaults = false
                if let results = try? privateContext.fetch(fetchRequest) {
                    let currency = results.first ?? CurrencyInfo(context: privateContext)
                    currency.currentPrice = currencyData.currentPrice
                    currency.shortName = currencyData.shortName
                    currency.priceChangeDay = currencyData.priceChangeDay
                }
            }
            try? privateContext.save()
            self.context.performAndWait {
                try? self.context.save()
            }
        }
    }

    func saveHistoricalDataOf(currency currencyName: String,
                              historicalData: HistoricalDataResponse,
                              forTimePeriod timePeriod: TimePeriod,
                              completion: (() -> Void)?) {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        privateContext.perform {
            let fetchRequest: NSFetchRequest<CurrencyHistoricalData> = CurrencyHistoricalData.fetchRequest()
            let predicate = NSPredicate(format: "shortName = %@ AND timePeriod = %@", currencyName, timePeriod.stringRepresentation)
            fetchRequest.predicate = predicate
            if let results = try? privateContext.fetch(fetchRequest),
                let cachedHistoricalData = results.first {
                for entry in historicalData.historyEntries {
                    cachedHistoricalData.appendTimestamp(timestamp: Timestamp(fromHistoryEnrty: entry, context: privateContext))
                }
            } else {
                _ = CurrencyHistoricalData(withHistoricalData: historicalData,
                                           context: privateContext,
                                           currencyName: currencyName,
                                           timePeriod: timePeriod.stringRepresentation)
            }
            do {
                try privateContext.save()
            } catch {
                print(error)
            }
            self.context.performAndWait {
                do {
                    self.context.refreshAllObjects()
                    try self.context.save()
                } catch {
                    print(error)
                }
            }
            DispatchQueue.main.async {
                completion?()
            }
        }
    }

    func amountOfTimestampsToLoadFor(currency currencyName: String, for timePeriod: TimePeriod, completion: ((Int) -> Void)?) {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        privateContext.perform {
            let fetchRequest: NSFetchRequest<CurrencyHistoricalData> = CurrencyHistoricalData.fetchRequest()
            let predicate = NSPredicate(format: "shortName = %@ AND timePeriod = %@", currencyName, timePeriod.stringRepresentation)
            fetchRequest.predicate = predicate
            if let results = try? privateContext.fetch(fetchRequest), let historicalData = results.first {
                self.removeOutdatedTimestamps(cachedHistoricalData: historicalData)
                try? privateContext.save()
                let result = self.amountOfTimestampsToKeep - (historicalData.timestamps?.array.count ?? 0)
                let amount = result > 0 ? result : result + 1
                DispatchQueue.main.async {
                    try? self.context.save()
                    completion?(amount)
                }
            } else {
                DispatchQueue.main.async {
                    completion?(self.amountOfTimestampsToKeep)
                }
            }
        }
    }

    func loadCurrencies(currencyNames: [String], completion: (([CurrencyData]) -> Void)?) {
        var currencies: [CurrencyData] = []
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        privateContext.perform {
            for currencyName in currencyNames {
                let fetchRequest: NSFetchRequest<CurrencyInfo> = CurrencyInfo.fetchRequest()
                let predicate = NSPredicate(format: "shortName = %@", currencyName)
                fetchRequest.predicate = predicate
                if let results = try? privateContext.fetch(fetchRequest) {
                    let cachedCurrency = results.first
                    let currency = CurrencyData(shortName: currencyName,
                                                currentPrice: cachedCurrency?.currentPrice ?? "---",
                                                priceChangeDay: cachedCurrency?.priceChangeDay ?? "---")
                    currencies.append(currency)
                }
            }
            DispatchQueue.main.async {
                completion?(currencies)
            }
        }
    }

    func loadHistoricalDataOf(currency currencyName: String, forTimePeriod timePeriod: TimePeriod, completion: (([HistoryEntry]) -> Void)?) {
        var cachedHistoryEntries: [HistoryEntry] = []
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        privateContext.perform {
            let fetchRequest: NSFetchRequest<CurrencyHistoricalData> = CurrencyHistoricalData.fetchRequest()
            let predicate = NSPredicate(format: "shortName = %@ AND timePeriod = %@", currencyName, timePeriod.stringRepresentation)
            fetchRequest.predicate = predicate
            if let results = try? privateContext.fetch(fetchRequest),
                let timestamps = results.first?.timestamps {
                for entry in timestamps {
                    if let timestamp = entry as? Timestamp {
                        cachedHistoryEntries.append(timestamp.asHistoryEntry())
                    }
                }
            }
            DispatchQueue.main.async {
                completion?(cachedHistoryEntries)
            }
        }
    }

    private func removeAllOutdatedTimestamps() {
        let fetchRequest: NSFetchRequest<CurrencyHistoricalData> = CurrencyHistoricalData.fetchRequest()
        if let results = try? context.fetch(fetchRequest) {
            for result in results {
                removeOutdatedTimestamps(cachedHistoricalData: result)
            }
        }
        try? context.save()
    }
    private func removeOutdatedTimestamps(cachedHistoricalData: CurrencyHistoricalData) {
        if let timePeriod = TimePeriod(string: cachedHistoricalData.timePeriod ?? "minute") {
            let timeInterval = timePeriod.rawValue * -Double(amountOfTimestampsToKeep)
            if let borderDate = Date().addingTimeInterval(timeInterval).truncateTo(timePeriod) {
                cachedHistoricalData.removeTimestampsBefore(date: borderDate)
            }
        }
    }
}
