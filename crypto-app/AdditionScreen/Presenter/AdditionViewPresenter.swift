//
//  AllCurrenciesCollectionPresenter.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/24/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//
import UIKit
import Kingfisher

class AdditionViewPresenter: AdditionViewPresenterProtocol {

    weak var additionView: AdditionViewProtocol? {
        didSet {
            self.additionView?.display(currencies: self.currencies)
        }
    }
    let model: CurrencyModelProtocol
    private var currencies: [CurrencyAdditionViewModel] = []

    init (withModel model: CurrencyModelProtocol) {
        self.model = model
    }

    func appendUserCurrencies(with currenciesData: [CurrencyAdditionViewModel]) {
        for currencyData in currenciesData {
            print(currencyData.currencyShortName)
            model.addTrackedCurrency(withShortName: currencyData.currencyShortName)
        }
        self.additionView?.goToPreviousScreen()
    }

    func requestCurrenciesUpdate(completion: (() -> Void)?) {
        self.model.getAvailableCurrenciesData { error, allCoinsResponse in
            if error == nil {
                if let retrievedData = allCoinsResponse?.allCoins {
                    var retrievedCurrencies = [CurrencyAdditionViewModel]()
                    for (currencyName, currencyData) in retrievedData where currencyData.isTrading {

                        let fullImageUrl: String?
                        if let baseUrl = allCoinsResponse?.baseImageUrl,
                           let relativeUrl = currencyData.imageUrl {
                            fullImageUrl = baseUrl + relativeUrl
                        } else {
                            fullImageUrl = nil
                        }

                        let currencyToDisplay = CurrencyAdditionViewModel(currencyShortName: currencyName,
                                                                          currencyFullName: currencyData.fullName,
                                                                          currencySymbol: currencyData.currencySymbol,
                                                                          imageUrl: fullImageUrl
                                                                        )
                        retrievedCurrencies.append(currencyToDisplay)
                    }
                    retrievedCurrencies.sort { $0.currencyShortName.lowercased() < $1.currencyShortName.lowercased() }
                    self.currencies = retrievedCurrencies
                    self.additionView?.display(currencies: retrievedCurrencies)
                }
            } else {
                //ask model to load local data
            }
            completion?()
        }
    }

    func cellWillAppear(cell: CurrencyListCell) {
        if let imageUrl = cell.currencyData?.imageUrl {
            let url = URL(string: imageUrl)
            cell.currencyImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "currencyPicturePlaceholder"))
        }
    }
}
