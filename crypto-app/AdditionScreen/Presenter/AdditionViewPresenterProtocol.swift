//
//  AllCurrenciesPresenterProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol AdditionViewPresenterProtocol {
    var additionView: AdditionViewProtocol? { get set }

    func requestCurrenciesUpdate(completion: (() -> Void)?)
    func appendUserCurrencies(with currenciesData: [CurrencyAdditionViewModel])
    func cellWillAppear(cell: CurrencyListCell)
}
