//
//  AdditionViewController.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit
import Kingfisher

class AdditionViewController: UIViewController, AdditionViewProtocol {

    @IBOutlet private var initialActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private var allCurrenciesCollectionView: UICollectionView!
    private var presenter: AdditionViewPresenterProtocol
    var cellNibName: String = "CurrencyListCell"
    var currenciesToDisplay: [CurrencyAdditionViewModel] = []
    var currenciesToAdd: [CurrencyAdditionViewModel] = []

    lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(AdditionViewController.refreshCurrencies),
                          for: .valueChanged)
        return refresh
    }()

    private let cellIdentifier = "DefaultCell"

    init(withPresenter presenter: AdditionViewPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "AdditionViewController", bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func display(currencies: [CurrencyAdditionViewModel]) {
        currenciesToDisplay = currencies
        allCurrenciesCollectionView.reloadData()
    }

    func setupCollectionView() {
        allCurrenciesCollectionView.refreshControl = refreshControl
        allCurrenciesCollectionView.register(UINib(nibName: cellNibName,
                                                   bundle: nil),
                                                   forCellWithReuseIdentifier: cellIdentifier)
        allCurrenciesCollectionView.allowsMultipleSelection = true
        self.navigationItem.title = "Available For Tracking"
    }

    func goToPreviousScreen() {
        self.navigationController?.popViewController(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.additionView = self
        setupCollectionView()
        self.presenter.requestCurrenciesUpdate {
            self.initialActivityIndicator.stopAnimating()
        }
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    @objc func refreshCurrencies() {
        self.presenter.requestCurrenciesUpdate {
            self.refreshControl.endRefreshing()
        }
    }

    @objc func addSelectedCurrencies(_ sender: UIBarButtonItem) {
        presenter.appendUserCurrencies(with: currenciesToAdd)
    }
}

extension AdditionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let indices = collectionView.indexPathsForSelectedItems {
            if indices.count == 1 {
                let addButton = UIBarButtonItem(title: "Add",
                                                style: .done,
                                                target: self,
                                                action: #selector(AdditionViewController.addSelectedCurrencies))
                self.navigationItem.setRightBarButton(addButton, animated: true)
            }
        }
        if let cell = allCurrenciesCollectionView.cellForItem(at: indexPath) as? CurrencyListCell {
            if let cellData = cell.currencyData {
                currenciesToAdd.append(cellData)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let indices = collectionView.indexPathsForSelectedItems {
            if indices.isEmpty {
                self.navigationItem.setRightBarButton(nil, animated: true)
            }
        }
        if let cell = allCurrenciesCollectionView.cellForItem(at: indexPath) as? CurrencyListCell {
            if let cellData = cell.currencyData {
                if let index = currenciesToAdd.firstIndex(where: { element in
                    element.currencyShortName == cellData.currencyShortName
                }) {
                    currenciesToAdd.remove(at: index)
                }
            }
        }
    }
}

extension AdditionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {

        return currenciesToDisplay.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                         for: indexPath) as? CurrencyListCell {
            guard indexPath.item < currenciesToDisplay.count else {
                return UICollectionViewCell()
            }
            let cellData = currenciesToDisplay[indexPath.item]
            cell.currencyData = cellData
            self.presenter.cellWillAppear(cell: cell)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
