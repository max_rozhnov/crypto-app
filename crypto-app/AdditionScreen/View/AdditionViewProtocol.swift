//
//  AdditionViewProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
protocol AdditionViewProtocol: class {
    var currenciesToDisplay: [CurrencyAdditionViewModel] { get set }
    func display(currencies: [CurrencyAdditionViewModel])
    func goToPreviousScreen()
}
