//
//  AdditionLayout.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import UIKit

class AdditionLayout: UICollectionViewFlowLayout {
    var itemsPerRow: CGFloat = 1.0

    override init() {
        super.init()
        initInsets()
    }

    private func initInsets() {
        let insetTop: CGFloat = 10.0
        let insetLeft: CGFloat = 5.0
        let insetBottom: CGFloat = 5.0
        let insetRight: CGFloat = 5.0
        self.sectionInset = UIEdgeInsets(top: insetTop,
                                         left: insetLeft,
                                         bottom: insetBottom,
                                         right: insetRight)
        let itemWidth = UIScreen.main.bounds.width / itemsPerRow - (insetLeft + insetRight)
        self.sectionInsetReference = .fromSafeArea
        self.itemSize = CGSize(width: itemWidth, height: itemWidth / 1.5)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init()
        initInsets()
    }
}
