//
//  CurrencyListCell.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/25/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit

class CurrencyListCell: UICollectionViewCell {
    @IBOutlet private var dataView: UIView!
    @IBOutlet private var selectionView: UIView!
    @IBOutlet private var currencyImageView: UIImageView!

    private let innerCornerRadius: CGFloat = 25.0
    private let outerCornerRadius: CGFloat = 33.0

    @IBOutlet private var currencyIndexLabel: UILabel!
    @IBOutlet private var currencyFullNameLabel: UILabel!

    public var currencyImage: UIImageView {
        return currencyImageView
    }

    public var currencyData: CurrencyAdditionViewModel? {
        didSet {
            currencyFullNameLabel.text = currencyData?.currencyFullName
            currencyIndexLabel.text = currencyData?.currencyShortName
        }
    }

    override var isSelected: Bool {
        didSet {
                selectionView.isHidden = !isSelected
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        isSelected = false
        dataView.layer.cornerRadius = innerCornerRadius
        selectionView.layer.cornerRadius = outerCornerRadius
    }
}
