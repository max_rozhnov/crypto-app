//
//  CurrencyAdditionViewModel.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/4/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct CurrencyAdditionViewModel {
    var currencyShortName: String
    var currencyFullName: String
    var currencySymbol: String
    var imageUrl: String?
}
