//
//  Constants.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/23/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

struct Constants {
    static let defaultAnimationDuration = 0.7
}
