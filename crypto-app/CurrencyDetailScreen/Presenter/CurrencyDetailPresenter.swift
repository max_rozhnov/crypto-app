//
//  CurrencyDetailPresenter.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/20/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import Charts

enum TimePeriod: Double {
    case minute = 60
    case hour = 3600
    case day = 86400

    var stringRepresentation: String {
        switch self {
        case .minute:
            return "minute"
        case .hour:
            return "hour"
        case .day:
            return "day"
        }
    }

    init?(string: String) {
        switch string {
        case "minute":
            self = .minute
        case "hour":
            self = .hour
        case "day":
            self = .day
        default:
            return nil
        }
    }
}

class CurrencyDetailPresenter: CurrencyDetailPresenterProtocol {
    let model: CurrencyModelProtocol
    private let currencyName: String
    private var firstChartEntryDate: Date?
    public weak var detailView: CurrencyDetailProtocol?
    private var selectedPeriod: TimePeriod = .minute

    init(withModel model: CurrencyModelProtocol, andCurrencyName currencyName: String) {
        self.model = model
        self.currencyName = currencyName
    }

    func requestData(forTimePeriod timePeriod: TimePeriod, completion: (() -> Void)?) {
        detailView?.startChartLoadingAnimation()
        selectedPeriod = timePeriod
        model.getHistoricalData(of: currencyName, for: timePeriod) { error, response in
            if error == nil {
                self.firstChartEntryDate = response?.historyEntries.first?.time
                if let historyData = response?.historyEntries {
                    let chartEntries = self.chartEntries(from: historyData)
                    let dataSet = CandleChartDataSet(values: chartEntries, label: nil)
                    self.detailView?.displayChartData(dataSet: dataSet)
                    self.detailView?.hideAlert()
                    if let currencyPrice = historyData.last?.close {
                        if timePeriod == .minute {
                            self.detailView?.currencyPrice = "\(currencyPrice) $US"
                        }
                    }
                }
            } else {
                self.detailView?.showAlert(withText: "Connection Error")
                self.model.historicalLocalData(of: self.currencyName, for: timePeriod) { cachedHistoryEntries in
                                    let chartEntries = self.chartEntries(from: cachedHistoryEntries ?? [])
                                    let dataSet = CandleChartDataSet(values: chartEntries, label: nil)
                                    self.detailView?.displayChartData(dataSet: dataSet)
                }
            }
            completion?()
            self.detailView?.stopChartLoadingAnimation()
        }
    }

    private func chartEntries(from historyEntries: [HistoryEntry]) -> [CandleChartDataEntry] {
        var chartEntries = [CandleChartDataEntry]()
        self.firstChartEntryDate = historyEntries.first?.time
        if let firstValue = historyEntries.first {
            let invisibleEntry = CandleChartDataEntry(x: 0,
                                                      shadowH: firstValue.high,
                                                      shadowL: firstValue.low,
                                                      open: firstValue.open,
                                                      close: firstValue.close)

            chartEntries.append(invisibleEntry)
            for historyEntry in historyEntries {
                let xValue = (historyEntry.time.timeIntervalSince1970 -
                    firstValue.time.timeIntervalSince1970) / self.selectedPeriod.rawValue
                let chartEntry = CandleChartDataEntry(x: xValue,
                                                      shadowH: historyEntry.high,
                                                      shadowL: historyEntry.low,
                                                      open: historyEntry.open,
                                                      close: historyEntry.close)
                chartEntries.append(chartEntry)
            }
        }

        return chartEntries
    }
}

extension CurrencyDetailPresenter: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let dateFormatter = DateFormatter()
        switch selectedPeriod {
        case .minute:
            dateFormatter.dateFormat = "HH:mm"
        case.hour:
            dateFormatter.dateFormat = "HH:mm"
        case.day:
            dateFormatter.dateFormat = "MM.dd"
        }
        if let firstEntryDate = firstChartEntryDate {
            let entryTime = firstEntryDate.timeIntervalSince1970 + (value * selectedPeriod.rawValue)
            return dateFormatter.string(from: Date(timeIntervalSince1970: entryTime))
        }
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
}
