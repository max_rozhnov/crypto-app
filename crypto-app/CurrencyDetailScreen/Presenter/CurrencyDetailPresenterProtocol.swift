//
//  CurrencyDetailPresenterProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/20/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol CurrencyDetailPresenterProtocol {
    var detailView: CurrencyDetailProtocol? { get set }
    func requestData(forTimePeriod timePeriod: TimePeriod, completion: (() -> Void)?)
}
