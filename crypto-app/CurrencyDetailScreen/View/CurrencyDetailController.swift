//
//  CurrencyDetailController.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/20/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit
import Charts

class CurrencyDetailController: UIViewController, CurrencyDetailProtocol {

    @IBOutlet private var alertTextLabel: UILabel!
    @IBOutlet private var alertConstraint: NSLayoutConstraint!
    @IBOutlet private var alertView: UIView!
    @IBOutlet private var priceInfoView: UIView!
    @IBOutlet private var chartLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet private var timePeriodSegmentedControl: UISegmentedControl!

    @IBAction private func timePeriodSelected(_ sender: Any) {
        timePeriodSegmentedControl.isUserInteractionEnabled = false
        switch timePeriodSegmentedControl.selectedSegmentIndex {
        case 0:
            timePeriodLabel.text = "this minute"
            presenter.requestData(forTimePeriod: .minute) {
                self.timePeriodSegmentedControl.isUserInteractionEnabled = true
            }
        case 1:
            timePeriodLabel.text = "this hour"
            presenter.requestData(forTimePeriod: .hour) {
                self.timePeriodSegmentedControl.isUserInteractionEnabled = true
            }
        case 2:
            timePeriodLabel.text = "today"
            presenter.requestData(forTimePeriod: .day) {
                self.timePeriodSegmentedControl.isUserInteractionEnabled = true
            }
        default:
            break
        }
    }

    @IBOutlet private var currentPriceLabel: UILabel!
    @IBOutlet private var priceChangeLabel: UILabel!
    @IBOutlet private var timePeriodLabel: UILabel!
    @IBOutlet private var currencyPriceNameLabel: UILabel!

    @IBOutlet private var candlestickChart: CandleStickChartView!

    public var currencyPrice: String? {
        get {
            return currentPriceLabel.text
        }
        set(newCurrencyPrice) {
            currentPriceLabel.text = newCurrencyPrice
        }
    }
    public var currencyPriceChange: String? {
        get {
            return priceChangeLabel.text
        }
        set(newPriceChange) {
            priceChangeLabel.text = newPriceChange
        }
    }
    public var currencyName: String? {
        get {
            return currencyPriceNameLabel.text
        }
        set(newCurrencyName) {
            if let currencyNameUnwrapped = newCurrencyName {
                self.navigationItem.title = currencyNameUnwrapped
                currencyPriceNameLabel.text = currencyNameUnwrapped + " Price"
            }
        }
    }

    private var presenter: CurrencyDetailPresenterProtocol

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.detailView = self
        setupChart()
        timePeriodSelected(self)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if size.width > size.height {
            candlestickChart.zoomToCenter(scaleX: 0.5, scaleY: 1)
            priceInfoView.isHidden = true
        } else {
            priceInfoView.isHidden = false
            candlestickChart.zoomToCenter(scaleX: 2, scaleY: 1)
        }
    }
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }

    init(withPresenter presenter: CurrencyDetailPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "CurrencyDetailController", bundle: nil)
    }

    func displayChartData(dataSet: CandleChartDataSet) {
        if !dataSet.isEmpty {
            formatDataSet(dataSet: dataSet)
            let chartData = CandleChartData(dataSet: dataSet)
            self.candlestickChart.data = chartData
        }
    }

    func startChartLoadingAnimation() {
        candlestickChart.isHidden = true
        chartLoadingIndicator.startAnimating()
    }

    func stopChartLoadingAnimation() {
        chartLoadingIndicator.stopAnimating()
        candlestickChart.isHidden = false
    }

    func showAlert(withText text: String) {
        alertTextLabel.text = text
        UIView.animate(withDuration: Constants.defaultAnimationDuration) {
            self.alertConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }

    func hideAlert() {
        if alertConstraint.constant == 0 {
            UIView.animate(withDuration: Constants.defaultAnimationDuration) {
                self.alertConstraint.constant = self.alertView.bounds.height
                self.view.layoutIfNeeded()
            }
        }
    }

    private func setupChart() {
        candlestickChart.xAxis.valueFormatter = presenter as? IAxisValueFormatter
        candlestickChart.noDataText = "No cached data for selected time period"
        candlestickChart.legend.enabled = false
        candlestickChart.autoScaleMinMaxEnabled = true
        candlestickChart.extraTopOffset = 0
        let leftAxis = candlestickChart.getAxis(.left)
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawAxisLineEnabled = false
        let rightAxis = candlestickChart.getAxis(.right)
        rightAxis.drawAxisLineEnabled = false
        candlestickChart.xAxis.drawGridLinesEnabled = false
        candlestickChart.xAxis.drawAxisLineEnabled = false
        candlestickChart.xAxis.labelPosition = .bottom
        candlestickChart.xAxis.labelTextColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)

        candlestickChart.setScaleEnabled(false)
        candlestickChart.autoScaleMinMaxEnabled = false

        candlestickChart.zoomToCenter(scaleX: 4, scaleY: 1)
    }

    private func formatDataSet(dataSet: CandleChartDataSet) {
        dataSet.shadowColorSameAsCandle = true
        dataSet.decreasingColor = #colorLiteral(red: 0.9364492868, green: 0.1229443967, blue: 0.07450980693, alpha: 1)
        dataSet.neutralColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        dataSet.increasingColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        dataSet.decreasingFilled = true
        dataSet.increasingFilled = true
        dataSet.barSpace = 0.1
        dataSet.drawValuesEnabled = false
        dataSet.drawHorizontalHighlightIndicatorEnabled = false
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
