//
//  CurrencyDetailProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/26/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import Charts

protocol CurrencyDetailProtocol: class {
    var currencyName: String? { get set }
    var currencyPrice: String? { get set }
    var currencyPriceChange: String? { get set }

    func displayChartData(dataSet: CandleChartDataSet)
    func startChartLoadingAnimation()
    func stopChartLoadingAnimation()
    func showAlert(withText text: String)
    func hideAlert()
}
