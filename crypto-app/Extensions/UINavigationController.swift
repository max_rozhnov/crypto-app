//
//  UINavigationControllerExtension.swift
//  crypto-app
//
//  Created by Max Rozhnov on 3/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import  UIKit

extension UINavigationController {

    override open var shouldAutorotate: Bool {
        if let visibleVC = visibleViewController {
            return visibleVC.shouldAutorotate
        }
        return super.shouldAutorotate
    }
}
