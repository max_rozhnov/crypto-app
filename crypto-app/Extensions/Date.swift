//
//  Date.swift
//  crypto-app
//
//  Created by Max Rozhnov on 4/16/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

extension Date {
    func truncateTo(_ timePeriod: TimePeriod) -> Date? {
        let calender = Calendar.current
        switch timePeriod {
        case .minute:
            let dateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute], from: self)
            return calender.date(from: dateComponents)
        case .hour:
            let dateComponents = calender.dateComponents([.year, .month, .day, .hour], from: self)
            return calender.date(from: dateComponents)
        case .day:
            let dateComponents = calender.dateComponents([.year, .month, .day], from: self)
            return calender.date(from: dateComponents)
        }
    }
}
