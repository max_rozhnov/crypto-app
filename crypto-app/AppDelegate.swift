//
//  AppDelegate.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/15/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let networkDataProvider = NetworkDataProvider()
        let localManager = LocalDataManager(with: self.persistentContainer.viewContext)
        let model = CurrencyModelRemote(networkDataProvider: networkDataProvider, localStorageManager: localManager)
        let presenter = CurrencyCollectionPresenter(withModel: model)
        let storyboard = UIStoryboard(name: "MainView", bundle: nil)
        if let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as? UINavigationController {
            if let currencyCollectionController = initialViewController.viewControllers.first as? CurrencyCollectionController {
                currencyCollectionController.presenter = presenter
                window?.rootViewController = initialViewController
                window?.makeKeyAndVisible()
                return true
            }
        }
        return false
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "crypto_app")
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
