//
//  ViewController.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/15/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit

class CurrencyCollectionController: UIViewController, CurrencyCollectionProtocol {
    func startChartLoadingAnimation() {
        currencyCollection.isHidden = true
        loadingIndicator.startAnimating()
    }

    func stopChartLoadingAnimation() {
        loadingIndicator.stopAnimating()
        currencyCollection.isHidden = false
    }

    private var currenciesToDisplay: [CurrencyData] = []
    var presenter: CurrencyCollectionPresenterProtocol?
    var cellNibName: String = "CurrencyCell"
    lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(CurrencyCollectionController.refreshCurrencies),
                          for: .valueChanged)
        return refresh
    }()

    private let cellIdentifier = "DefaultCell"

    @IBOutlet private var alertConstraint: NSLayoutConstraint!
    @IBOutlet private var alertTextLabel: UILabel!
    @IBOutlet private var alertView: UIView!
    @IBOutlet private var currencyCollection: UICollectionView!
    @IBOutlet private var loadingIndicator: UIActivityIndicatorView!
    @IBAction private func addButtonAction(_ sender: Any) {
        presenter?.navigateToAdditionView()
    }

    func setupCollectionView() {
        currencyCollection.delegate = self
        currencyCollection.dataSource = self
        currencyCollection.refreshControl = refreshControl
        currencyCollection.register(UINib(nibName: cellNibName, bundle: nil),
                                    forCellWithReuseIdentifier: cellIdentifier)
    }

    func showAlert(withText text: String) {
        alertTextLabel.text = text
        UIView.animate(withDuration: Constants.defaultAnimationDuration) {
            self.alertConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }

    func hideAlert() {
        if alertConstraint.constant == 0 {
            UIView.animate(withDuration: Constants.defaultAnimationDuration) {
                self.alertConstraint.constant = self.alertView.bounds.height
                self.view.layoutIfNeeded()
            }
        }
    }

    func transition(to controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }

    func display(currencies: [CurrencyData]) {
        currenciesToDisplay = currencies
        currencyCollection.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.currencyCollectionView = self
        setupCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.requestCurrenciesUpdate {
            self.loadingIndicator?.stopAnimating()
        }
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    @objc func refreshCurrencies() {
        presenter?.requestCurrenciesUpdate {
            self.refreshControl.endRefreshing()
        }
    }
}

extension CurrencyCollectionController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if let cell = currencyCollection.cellForItem(at: indexPath) as? CurrencyCell {
            if let currencyShortName = cell.currencyIndex {
                presenter?.navigateToDetailView(forCurrency: currencyShortName)
            }
        }
    }
}

extension CurrencyCollectionController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return currenciesToDisplay.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                         for: indexPath) as? CurrencyCell {
            let celldata = currenciesToDisplay[indexPath.item]
            cell.currencyIndex = celldata.shortName
            cell.currencyPrice = celldata.currentPrice
            cell.currencyPriceChange = celldata.priceChangeDay
            return cell
        } else {
            return CurrencyCell()
        }
    }
}
