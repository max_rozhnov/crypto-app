//
//  CurrencyCell.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/19/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit

class CurrencyCell: UICollectionViewCell {

    private let cornerRadius: CGFloat = 25.0

    @IBOutlet private var currencyIndexLabel: UILabel!
    @IBOutlet private var currencyPriceChangeLabel: UILabel!
    @IBOutlet private var currencyPriceLabel: UILabel!

    public var currencyIndex: String? {
        get {
            return currencyIndexLabel.text
        }
        set(newCurrencyIndex) {
            currencyIndexLabel.text = newCurrencyIndex
        }
    }
    public var currencyPriceChange: String? {
        get {
            return currencyPriceChangeLabel.text
        }
        set(newCurrencyPriceChange) {
            currencyPriceChangeLabel.text = newCurrencyPriceChange
        }
    }
    public var currencyPrice: String? {
        get {
            return currencyPriceLabel.text
        }
        set(newCurrencyPrice) {
            currencyPriceLabel.text = newCurrencyPrice
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor(red: 0.0, green: 0.5, blue: 1.0, alpha: 1.0)
        self.contentView.layer.cornerRadius = cornerRadius
    }
}
