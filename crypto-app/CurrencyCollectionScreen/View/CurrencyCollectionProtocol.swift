//
//  CurrencyCollectionProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/27/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation
import UIKit

protocol CurrencyCollectionProtocol: class {
    func transition(to controller: UIViewController)
    func display(currencies: [CurrencyData])
    func startChartLoadingAnimation()
    func stopChartLoadingAnimation()
    func showAlert(withText text: String)
    func hideAlert()
}
