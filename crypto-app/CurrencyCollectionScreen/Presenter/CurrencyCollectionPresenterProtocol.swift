//
//  CurrencyCollectionPresenterProtocol.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/18/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

protocol CurrencyCollectionPresenterProtocol {
    var currencyCollectionView: CurrencyCollectionProtocol? { get set }

    func requestCurrenciesUpdate(completion: (() -> Void)?)
    func navigateToDetailView(forCurrency currencyShortName: String)
    func navigateToAdditionView()
}
