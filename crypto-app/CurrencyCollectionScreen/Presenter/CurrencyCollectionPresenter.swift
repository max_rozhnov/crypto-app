//
//  CurrencyCollectionPresenter.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/18/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import Foundation

class CurrencyCollectionPresenter: CurrencyCollectionPresenterProtocol {

    let model: CurrencyModelProtocol
    var amountOfItems: Int {
            return currencies.count
    }
    private var currencies: [CurrencyData] = []
    weak var currencyCollectionView: CurrencyCollectionProtocol?

    init (withModel model: CurrencyModelProtocol) {
        self.model = model
    }

    func requestCurrenciesUpdate(completion: (() -> Void)?) {
        self.currencyCollectionView?.startChartLoadingAnimation()
        model.getCurrenciesData { error, data in
            if error != nil {
                self.currencyCollectionView?.showAlert(withText: "Connection Error")
                self.model.currenciesLocalData { cachedCurrenciesData in
                    self.currencies = cachedCurrenciesData ?? []
                }
            } else if let retrievedData = data?.display {
                var retrievedCurrencies = [CurrencyData]()
                for (currencyName, currencyData) in retrievedData {
                    if let currencySpecificData = currencyData[self.model.appCurrency] {
                        let currencyPrice = currencySpecificData.price
                        let currencyPriceChange = currencySpecificData.changeDay
                        let currencyToAdd = CurrencyData(shortName: currencyName,
                                                         currentPrice: currencyPrice,
                                                         priceChangeDay: currencyPriceChange )
                        retrievedCurrencies.append(currencyToAdd)
                    }
                }
                self.currencyCollectionView?.hideAlert()
                self.currencies = retrievedCurrencies
            }
            self.currencies.sort { $0.shortName < $1.shortName }
            self.currencyCollectionView?.display(currencies: self.currencies)
            self.currencyCollectionView?.stopChartLoadingAnimation()
            completion?()
        }
    }

    func navigateToDetailView(forCurrency currencyShortName: String) {
        let detailPresenter = CurrencyDetailPresenter(withModel: model, andCurrencyName: currencyShortName)
        let currencyDetail = CurrencyDetailController(withPresenter: detailPresenter)
        currencyCollectionView?.transition(to: currencyDetail)
    }
    func navigateToAdditionView() {
        let additionViewPresenter = AdditionViewPresenter(withModel: model)
        let additionView = AdditionViewController(withPresenter: additionViewPresenter)
        currencyCollectionView?.transition(to: additionView)
    }
}
