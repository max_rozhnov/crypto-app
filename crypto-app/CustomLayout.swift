//
//  CustomLayout.swift
//  crypto-app
//
//  Created by Max Rozhnov on 2/25/19.
//  Copyright © 2019 Max Rozhnov. All rights reserved.
//

import UIKit

class CustomLayout: UICollectionViewFlowLayout {
    var itemsPerRow: CGFloat = 2.0

    required init?(coder aDecoder: NSCoder) {
        super.init()
    }

    override func prepare() {
        guard let cv = collectionView else { return }
        let insetTop: CGFloat = 10.0
        let insetLeft: CGFloat = 5.0
        let insetBottom: CGFloat = 5.0
        let insetRight: CGFloat = 5.0
        self.sectionInset = UIEdgeInsets(top: insetTop,
                                         left: insetLeft,
                                         bottom: insetBottom,
                                         right: insetRight)
        let itemWidth = cv.bounds.width / 2 - (insetLeft + insetRight)
        self.sectionInsetReference = .fromSafeArea
        self.itemSize = CGSize(width: itemWidth, height: itemWidth / 1.5)
    }
}
